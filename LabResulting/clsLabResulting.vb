Imports System
Imports System.Configuration
Imports SMTPEmailer.Emailer
Imports System.Tuples

Public Class clsLabResulting
#Region "Nested class for test order translation variables"
    Private Class xlateTestOrder
        Public ReturnCode As String
        Public Reflex As Boolean
    End Class
#End Region

#Region "Private Variables"
    Private objADOMessages As CP_ADO2005.clsADO
    Private objADOResults As CP_ADO2005.clsADO
    Private objResults As CP_ADO2005.clsADO
    Private strResultsConnect As String = String.Empty '"Provider=SQLOLEDB.1;Password=stranded;Persist Security Info=True;User ID=appusers;Initial Catalog=LISResults;Data Source=vcpatestdb"
    Private strCunninghamConnect As String = String.Empty 'Provider=SQLOLEDB.1;Password=stranded;Persist Security Info=True;User ID=appusers;Initial Catalog=Cunningham;Data Source=vcpatestdb"

    Private strDirectoryPoll As String = "\\vcpatestdb\E$\Interfaces\Bernhardt\Results\"
    Private strDirectoryProcessed As String = "\\vcpatestdb\E$\Interfaces\Bernhardt\Archive\Results\"
    Private strDirectoryErrors As String = "\\vcpatestdb\E$\Interfaces\Bernhardt\Archive\ErrorsResults\"
    Private lstStatus As List(Of String)
    Private bolEmailSent As Boolean = False
    Private bolUpdateResultsOnly = False
    Private lastInsert As Tuples.Tuple(Of String, String)




#End Region

#Region "Events"
    Public Event StatusUpdate(ByVal intCount As Integer, ByVal strDesc As String)
    Public Event StatusStart(ByVal intTotal As Integer)
    Public Event StatusEnd()
#End Region

#Region "ADO Connect/Disconnects"
    Private Function ConnectDatabase(ByRef objADO As CP_ADO2005.clsADO, ByVal strConnection As String) As Boolean
        Dim rtn As Boolean
        Try
            objADO = New CP_ADO2005.clsADO
            objADO.ProviderType = CP_ADO2005.clsADO.eProvType.enuOLEDB
            objADO.Connection_String = strConnection
            objADO.PersistConnection()
            rtn = True
        Catch ex As Exception
            rtn = False
        End Try
        Return rtn
    End Function

    Private Function ConnectDatabase(ByRef objADO As CP_ADO2005.clsADO, ByVal strConnection As String, ByVal provider As CP_ADO2005.clsADO.eProvType) As Boolean
        Dim rtn As Boolean
        Try
            objADO = New CP_ADO2005.clsADO
            objADO.ProviderType = provider
            objADO.Connection_String = strConnection
            objADO.PersistConnection()
            rtn = True
        Catch ex As Exception
            rtn = False
        End Try
        Return rtn
    End Function

    Private Sub DisconnectDatabase(ByRef objADO As CP_ADO2005.clsADO)
        objADO.Disconnect()
        objADO = Nothing
    End Sub
#End Region

#Region "Polling Routines Called from Timers"
    Public Sub PollDatabase()

        Dim dtMessages As DataTable

        If ConnectDatabase(objADOMessages, strResultsConnect) Then
            dtMessages = ReturnMessageList()
            RaiseEvent StatusStart(dtMessages.Rows.Count)
            If dtMessages.Rows.Count > 0 Then
                ProcessMessages(dtMessages)
            End If
            dtMessages.Dispose()
            DisconnectDatabase(objADOMessages)
            RaiseEvent StatusEnd()
        End If

    End Sub
    Public Sub PollFiles()
        bolEmailSent = False
        Dim strFiles() As String = ReturnMessageList(strDirectoryPoll)
        RaiseEvent StatusStart(strFiles.Length)
        If strFiles.Length > 0 Then
            For I As Integer = 0 To strFiles.Length - 1
                RaiseEvent StatusUpdate(I + 1, "Processing " & strFiles(I) & "...")
                ProcessMessage(strFiles(I))
                Debug.Print(strFiles(I))
            Next
        End If
        RaiseEvent StatusEnd()
    End Sub
    Private Function ReturnMessageList() As DataTable
        Dim rtn As DataTable
        Dim strSQL As String = "Select MSH_ID from tblMSH where Processed = 0 "
        rtn = objADOMessages.GetDataTable(strSQL, "MSH")
        Return rtn
    End Function
    Private Function ReturnMessageList(ByVal strSearchPath As String) As String()
        Dim strReturn As String() = System.IO.Directory.GetFiles(strSearchPath)
        Return strReturn
    End Function
#End Region

#Region "Process Messages File or Database"
    Private Sub ProcessMessages(ByVal dtMSHListing As DataTable)

        For I As Integer = 0 To dtMSHListing.Rows.Count - 1
            RaiseEvent StatusUpdate(I + 1, "Processing MSH ID: " & CInt(dtMSHListing.Rows(I)!MSH_ID))
            ProcessMessage(CInt(dtMSHListing.Rows(I)!MSH_ID))
        Next
    End Sub
    Private Sub ProcessMessage(ByVal intMSH As Integer)
        Dim objRecords As clsResultRecords = ReturnTestElements(intMSH)
        ProcessBHLWithUpdate(objRecords, intMSH)
    End Sub
    Private Sub ProcessMessage(ByVal strFile As String)
        Dim objRecords As clsResultRecords = ReturnTestElements(strFile)

        ProcessBHLWithUpdate(objRecords, strFile)

        Dim len As Integer = strFile.LastIndexOf("\") + 1
        Dim strFileName As String = strFile.Substring(len)
        lstStatus = New List(Of String)
        If UpdateHL7Results(objRecords, strFileName) = True Then
            WriteLogFile(String.Format("{0} : All Records processed for {1}!", Now.ToLongTimeString, strFileName))
        Else
            WriteLogFile("**************************************************************************")
            WriteLogFile(String.Format("{0} : Not All Records for {1} Process!", Now.ToLongTimeString, strFileName))
            For Each statusRec As String In lstStatus
                WriteLogFile(statusRec)
            Next
            WriteLogFile("**************************************************************************")

        End If
    End Sub


    Private Sub ProcessBHLWithUpdate(ByRef objRecords As clsResultRecords, ByVal strFile As String)
        Dim intReturn As Integer '= 2 'Remove = 2 when done testing
        Dim strDirectoryCopyTo As String = String.Empty
        intReturn = ProcessResults(objRecords) 'Activate this line after testing
        If intReturn > -1 Then
            Select Case intReturn
                Case 1
                    strDirectoryCopyTo = strDirectoryProcessed
                Case 2
                    strDirectoryCopyTo = ""
                Case Else
                    strDirectoryCopyTo = strDirectoryErrors
                    MoveReportedErrors(strDirectoryCopyTo)
                    'Case Else
                    '    strDirectoryCopyTo = strDirectoryErrors
                    '    MoveReportedErrors()
            End Select
            If strDirectoryCopyTo <> "" Then
                If System.IO.File.Exists(strDirectoryCopyTo & System.IO.Path.GetFileName(strFile)) = False Then
                    System.IO.File.Move(strFile, strDirectoryCopyTo & System.IO.Path.GetFileName(strFile))
                Else
                    System.IO.File.Delete(strFile)
                End If
            End If
            If strDirectoryCopyTo = strDirectoryErrors Then
                Dim count As Integer = System.IO.Directory.GetFiles(strDirectoryErrors).Length
                If count > 0 Then
                    If bolEmailSent = False Then
                        'SendEmail()
                        bolEmailSent = True
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub MoveReportedErrors(ByVal strDirectoryCopyTo As String)
        For Each strFile As String In System.IO.Directory.GetFiles(strDirectoryCopyTo)
            If System.IO.File.Exists(strDirectoryCopyTo & "ReportedErrors\" & System.IO.Path.GetFileName(strFile)) = False Then
                System.IO.File.Move(strFile, strDirectoryCopyTo & "ReportedErrors\" & System.IO.Path.GetFileName(strFile))
            Else
                System.IO.File.Delete(strFile)
            End If
        Next
    End Sub


    Private Sub ProcessResultsWithUpdate(ByRef objRecords As clsResultRecords, ByVal intMSHID As Integer)
        Dim intReturn As Integer
        intReturn = ProcessResults(objRecords)
        If intReturn > 0 Then
            'ConnectDatabase(objADOMessages, strConnect)

            'Remove comment tag from below line after Testing
            If bolUpdateResultsOnly = False Then
                objADOMessages.Execute("Update tblMSH Set Processed = " & intReturn & " Where MSH_ID = " & intMSHID)
            End If
            'WriteLogFile("MSH ID: " & intMSHID & " - ProcessFlag: " & intReturn)
            'DisconnectDatabase(objADOMessages)
        End If
    End Sub
    Private Function ProcessResults(ByVal objRecords As clsResultRecords) As Integer
        Dim intMSHStatus As Integer = -1
        'Dim intAccessionID As Integer
        Dim T As Tuples.Tuple(Of Integer, String)

        If objRecords.Count > 0 Then
            If ConnectDatabase(objADOResults, strCunninghamConnect) Then

                For I As Integer = 0 To objRecords.Count - 1
                    If objRecords(I).UniqAccessionID = 0 And objRecords(I).AccessionNumber <> "" Then
                        T = GetUniqAccessionID(objRecords(I).AccessionNumber)
                        If T <> Nothing Then
                            objRecords(I).UniqAccessionID = T.Element1
                            objRecords(I).AccessionNumber = T.Element2
                        End If
                    End If
                    If objRecords(I).UniqAccessionID = 0 And objRecords(I).AccessionNumber = "" Then
                        objRecords(I).ResultValue = 2
                    Else
                        'need to get a match is it panel or is it test
                        PopulatePanelTestFromXREF(objRecords(I))
                        'This grabs the negative or positive from the NTE and places it in its own value to be used later
                        If objRecords(I).ResultText.Trim() <> "" And objRecords(I).ResultText.Contains(" ") Then
                            objRecords(I).ResultValue = objRecords(I).ResultText.Substring(0, objRecords(I).ResultText.IndexOf(" ")).Trim()
                        Else
                            objRecords(I).ResultValue = objRecords(I).ResultText
                        End If
                        'Update the database with the Test Record information from objRecord.Tests
                        UpdateResultsFromResultRecordList(objRecords(I))
                    End If

                Next
                For I As Integer = 0 To objRecords.Count - 1
                    Select Case objRecords(I).UpdateStatus
                        Case 1 'Processed no problem
                            If intMSHStatus < 2 Then
                                intMSHStatus = objRecords(I).UpdateStatus
                            End If
                        Case 2
                            If intMSHStatus <> 5 Then
                                intMSHStatus = objRecords(I).UpdateStatus
                            End If
                        Case 3, 4
                            If intMSHStatus <> 2 And intMSHStatus <> 5 Then
                                intMSHStatus = objRecords(I).UpdateStatus
                            End If
                        Case Else
                            intMSHStatus = objRecords(I).UpdateStatus
                    End Select
                Next

                'Now to loop through and get the overall result
                DisconnectDatabase(objADOResults)
            End If
            'If intMSHStatus <> 1 Then
            'Stop
            'End If


            For o As Integer = 0 To objRecords.Count - 1
                If objRecords(o).UpdateStatus = 0 Then
                    intMSHStatus = 0
                    WriteLogFile("UniqAccessionID =^ " & objRecords(o).UniqAccessionID & " ^ Updating Accession " & objRecords(0).AccessionNumber & " With ProcessingStatus " & intMSHStatus)

                    Exit For
                    intMSHStatus = objRecords(o).UpdateStatus
                End If
            Next

        End If

        Return intMSHStatus

    End Function
    Private Sub UpdateResultsFromResultRecordList(ByRef objRecord As clsResultRecord)
        'First Match up the Test in the tblAccessionClnPending...

        Dim strSelectOrderedTest As String
        Dim clnCategoryId As String = ConfigurationManager.AppSettings.Item("ClnCategoryId")
        Dim dtClnPending As DataTable

        'This will grab everything that was ordered on the accession for Bernhardt labs .
        'WriteLogFile("Updating Accession: " & objRecord.AccessionNumber)
        strSelectOrderedTest = "Select UniqPendingID,BatchID,ClnGroupTestID,ClnTestID from tblAccessionClinPending Where UniqAccessionID = " & objRecord.UniqAccessionID & " and Cancelled = 0 and ClnCategoryID = '" + clnCategoryId + "' AND ClnTestID = '@@TEST'"
        For I As Integer = 0 To objRecord.TestsResulted.Count - 1
            dtClnPending = objADOResults.GetDataTable(strSelectOrderedTest.Replace("@@TEST", objRecord.TestsResulted(I)))
            If dtClnPending.Rows.Count > 0 Or objRecord.AllowRefex = True Then
                'We have a match so take the uniqpending id and perform a simple update statement
                'Later, we need to use the test result xref to update the clnpending with the properly interfaced result.
                If dtClnPending.Rows.Count = 0 And objRecord.AllowRefex = True Then
                    'Order does not exist in Lab system but this is a reflex so create order then result.

                    CreateLabOrder(objRecord, objRecord.TestsResulted(I))
                    dtClnPending = objADOResults.GetDataTable(strSelectOrderedTest.Replace("@@TEST", objRecord.TestsResulted(I)))
                End If
                If dtClnPending.Rows.Count > 0 Then
                    UpdateDatabaseResult(dtClnPending.Rows(0)!UniqPendingID, dtClnPending.Rows(0)!ClnTestID, objRecord.ResultValue)
                End If
            End If

        Next
        For I As Integer = 0 To objRecord.TestsNegative.Count - 1
            dtClnPending = objADOResults.GetDataTable(strSelectOrderedTest.Replace("@@TEST", objRecord.TestsNegative(I)))
            If dtClnPending.Rows.Count > 0 Or objRecord.AllowRefex = True Then
                'We have a match so take the uniqpending id and perform a simple update statement
                'Later, we need to use the test result xref to update the clnpending with the properly interfaced result.
                If dtClnPending.Rows.Count = 0 And objRecord.AllowRefex = True Then
                    'Order does not exist in Lab system but this is a reflex so create order then result.
                    CreateLabOrder(objRecord, objRecord.TestsNegative(I))
                    dtClnPending = objADOResults.GetDataTable(strSelectOrderedTest.Replace("@@TEST", objRecord.TestsNegative(I)))
                End If
                If dtClnPending.Rows.Count > 0 Then
                    UpdateDatabaseResult(dtClnPending.Rows(0)!UniqPendingID, dtClnPending.Rows(0)!ClnTestID, "NEGATIVE")
                End If
            End If

        Next

    End Sub
    Private Function CreateLabOrder(ByRef objRecord As clsResultRecord, ByVal strTest As String) As Boolean
        'usp_saveClinicalTestsPending
        Dim pReturn As New CP_ADO2005.clsParameters
        Dim blnReturn As Boolean
        Try


            pReturn.Add("BatchID", DbType.AnsiString, ParameterDirection.Input, 30, "")
            pReturn.Add("UniqAccessionID", DbType.Int32, ParameterDirection.Input, 0, objRecord.UniqAccessionID)
            pReturn.Add("clnResultsSequence", DbType.Int16, ParameterDirection.Input, 0, 1)
            pReturn.Add("ClnCategoryID", DbType.AnsiString, ParameterDirection.Input, 5, ConfigurationManager.AppSettings.Item("ClnCategoryId"))
            pReturn.Add("ClnGroupTestID", DbType.AnsiString, ParameterDirection.Input, 10, objRecord.Panel)
            pReturn.Add("ClnTestID", DbType.AnsiString, ParameterDirection.Input, 10, strTest)
            pReturn.Add("DateOrdered", DbType.AnsiString, ParameterDirection.Input, 24, Now.ToString("MM/dd/yyyy hh:mm"))
            pReturn.Add("OrderedBy", DbType.AnsiString, ParameterDirection.Input, 10, "REFLEX")
            pReturn.Add("ResultsID", DbType.AnsiString, ParameterDirection.Input, 10, "")
            pReturn.Add("ResultsDesc", DbType.AnsiString, ParameterDirection.Input, 30, "")
            pReturn.Add("DateResulted", DbType.AnsiString, ParameterDirection.Input, 24, "01/01/1900")
            pReturn.Add("ResultedBY", DbType.AnsiString, ParameterDirection.Input, 10, "")
            pReturn.Add("Finalized", DbType.[Boolean], ParameterDirection.Input, 0, False)
            pReturn.Add("DateFinalized", DbType.AnsiString, ParameterDirection.Input, 24, "01/01/1900")
            pReturn.Add("FinalizedBy", DbType.AnsiString, ParameterDirection.Input, 10, "")
            pReturn.Add("Cancelled", DbType.[Boolean], ParameterDirection.Input, 0, False)
            pReturn.Add("cancelledby", DbType.AnsiString, ParameterDirection.Input, 10, "")
            pReturn.Add("DateCancelled", DbType.AnsiString, ParameterDirection.Input, 24, "01/01/1900")
            pReturn.Add("addendum", DbType.Boolean, ParameterDirection.Input, 0, False)
            pReturn.Add("ReturnVal", DbType.Int32, ParameterDirection.InputOutput, 0, 0)
            pReturn.Add("SkipHPVUpdate", DbType.Boolean, ParameterDirection.Input, 0, True)

            objADOResults.ClearErrors()
            objADOResults.ClearParameters()
            objADOResults.clsParameters = pReturn
            objADOResults.ExecuteStoredProcedure("usp_saveClinicalTestsPending")
            blnReturn = True
        Catch ex As Exception
            blnReturn = False
        End Try
        Return blnReturn
    End Function

    Private Function UpdateHL7Results(ByRef objRecords As clsResultRecords, ByVal fileName As String) As Boolean
        Dim blnReturn As Boolean
        Dim strInsertIntoOBR As String
        Dim recCount As Integer = objRecords.Count
        Dim blhRecord As New clsResultRecord()

        strInsertIntoOBR =
            "INSERT INTO [LISResults].[dbo].[tblOBR] ([PID_ID],[FillerOrderNumber],[ResultStatus]) "
        strInsertIntoOBR += "VALUES({0}, '{1}', '{2}')"

        Try

            If objResults Is Nothing Then
                objResults = New CP_ADO2005.clsADO()
            End If
            If Not objResults.blnConnected Then
                ConnectDatabase(objResults, strResultsConnect)
            End If

            Dim counter As Integer
            If objResults.blnConnected Then
                'Dim ID As Integer
                'Integer.TryParse(ds.Tables(tbl).Rows(0)(0).ToString(), initialCount)
                For recIndex As Integer = 0 To recCount - 1
                    blhRecord = objRecords(recIndex)

                    Dim t As Tuples.Tuple(Of String, String)
                    t = Tuples.Tuple.[New](blhRecord.FillerOrderNumber, blhRecord.AccessionNumber)
                    If t <> lastInsert Then
                        Dim strInsertSQL = String.Format(strInsertIntoOBR, blhRecord.PID_Id, blhRecord.FillerOrderNumber, blhRecord.AccessionNumber)
                        Dim val As Integer = InsertHL7Record(objResults, strInsertSQL, fileName)

                        If val = 1 Then
                            counter += 1
                        End If
                        lastInsert = t
                    End If
                Next
            End If

            If counter = recCount Then
                blnReturn = True
            End If

            blnReturn = True
        Catch ex As Exception
            blnReturn = False
        End Try

        Return blnReturn
    End Function

    Private Function InsertHL7Record(ByRef objResults As CP_ADO2005.clsADO, ByVal strInsertSQL As String, ByVal fileName As String) As Integer
        Dim ID As Integer = -1
        Dim retVal As Integer = 0
        Try

            objResults.Execute(strInsertSQL)
            Dim strIdentity = objResults.GetDataTable("SELECT IDENT_CURRENT('tblOBR')").Rows(0)(0).ToString()
            Integer.TryParse(strIdentity, ID)
            If ID > 0 Then
                retVal = 1
            Else
                retVal = 0
                lstStatus.Add(String.Format("OBR Record Id #{0} For {1} failed to process!", strIdentity, fileName))
            End If
        Catch ex As Exception

        End Try
        Return retVal
    End Function

    Private Function UpdateDatabaseResult(ByVal intUniqPendingID As Integer, ByVal strClnTestID As String, ByVal strRawTestValue As String) As Boolean
        Dim blnReturn As Boolean
        Dim strResultID As String
        Dim strResultDesc As String
        Dim ResultedBy As String = "Auto"
        Dim strUpdatePendingSQL As String = "Update tblAccessionClinPending Set ResultsID = '{0}',ResultsDesc = '{1}', DateResulted = GetDate(), ResultedBy = '{2}' Where Cancelled = 0 and Finalized = 0 and UniqPendingID = {3}"
        Dim strUpdateBatchSQL As String = "Update tblAccessionClinBatch Set ResultsID = '{0}',ResultsDesc = '{1}', DateResulted = GetDate(), ResultedBy = '{2}' Where UniqPendingID = {3}"
        Dim strSQLPending As String
        Dim strSQLBatch As String

        Select Case strRawTestValue.ToUpper
            Case "POSITIVE", "POS"
                strResultID = "Pos"
                strResultDesc = "Positive"
            Case Else
                strResultID = "Neg"
                strResultDesc = "Negative"
        End Select

        strSQLPending = String.Format(strUpdatePendingSQL, strResultID, strResultDesc, ResultedBy, intUniqPendingID)
        'strSQLPending = strUpdatePendingSQL.Replace("@@RID", strResultID)
        'strSQLPending = strSQLPending.Replace("@@DESC", strResultDesc)
        strSQLBatch = String.Format(strUpdateBatchSQL, strResultID, strResultDesc, ResultedBy, intUniqPendingID)
        'strSQLBatch = strUpdateBatchSQL.Replace("@@RID", strResultID)
        'strSQLBatch = strSQLBatch.Replace("@@DESC", strResultDesc)

        Try
            If bolUpdateResultsOnly = False Then
                objADOResults.Execute(strSQLPending) 'Remove comment tag after Testing
                objADOResults.Execute(strSQLBatch) ' Remove comment tag after Testing
            End If
            blnReturn = True
        Catch ex As Exception
            blnReturn = False
        End Try


        Return blnReturn
    End Function
#End Region

#Region "Gather Test Elements from MSH File or Database"
    Private Function ReturnTestElements(ByVal strFile As String) As clsResultRecords
        Dim strMessage As String() = System.IO.File.ReadAllLines(strFile)
        Dim strLine() As String
        Dim objRecord As clsResultRecord = Nothing
        Dim objRecords As New clsResultRecords
        Dim strPLastName As String = ""
        Dim strPFirstName As String = ""
        Dim strDOB As String = ""
        Dim strAlternateResultText As String = ""

        objRecords.strFileName = strFile
        For I As Integer = 0 To strMessage.Length - 1

            If strMessage(I).StartsWith("OBR") Then
                objRecord = New clsResultRecord
                strLine = strMessage(I).Split("|")
                objRecord.PID_Id = -50
                objRecord.FillerOrderNumber = strLine(3)
                objRecord.TestPanelIdentifier = strLine(4)
                objRecord.AccessionNumber = strLine(25)
                If objRecord.AccessionNumber = "" And strPLastName <> "" And strDOB <> "" Then
                    objRecord.AccessionNumber = ReturnAccessionNumberForPatient(strPLastName, strPFirstName, strDOB, ConfigurationManager.AppSettings.Item("ClnCategoryId"))
                End If
                strAlternateResultText = ""
            ElseIf strMessage(I).StartsWith("PID") Then
                strLine = strMessage(I).Split("|")
                If strLine.Length > 7 Then
                    Dim strPatientName As String = strLine(5)
                    Dim aryPatientName() As String = strPatientName.Split("^")
                    If aryPatientName.Length > 1 Then
                        strPLastName = aryPatientName(0)
                        strPFirstName = aryPatientName(1)
                    End If
                    Dim strBDateFormal As String = strLine(7)
                    If strBDateFormal.Length >= 8 Then
                        strDOB = strBDateFormal.Substring(4, 2) & "/" & strBDateFormal.Substring(6, 2) & "/" & strBDateFormal.Substring(0, 4)
                    End If
                End If
            ElseIf strMessage(I).StartsWith("NTE|1") Then 'Assume all NTE 1 have the result value
                strLine = strMessage(I).Split("|")
                If Not objRecord Is Nothing Then
                    objRecord.ResultText = strLine(5)
                    If objRecord.ResultText.Trim() = "" Then
                        objRecord.ResultText = strAlternateResultText
                    End If
                    objRecords.Add(objRecord)
                End If
            ElseIf strMessage(I).StartsWith("OBX") Then
                strLine = strMessage(I).Split("|")
                strAlternateResultText = strLine(5)
            End If

        Next

        Return objRecords

        'AccessionNumber, Panel, Test Code,
    End Function
    Private Function ReturnAccessionNumberForPatient(ByVal strLastname As String, ByVal strFirstName As String, ByVal strDOB As String, ByVal strClnCategoryId As String) As String
        Dim blnConnected As Boolean = False
        Dim strReturn As String = ""
        Dim PatientAccessionLookupSQL = "SELECT DISTINCT tblAccession.PrefixID + tblAccession.TestingLocationID + tblAccession.PlaceOfServiceID + tblAccession.CaseYear + tblAccession.AccessionSequence AS AccessionNumber,  tblAccession.UniqAccessionID FROM         tblAccessionClinPending INNER JOIN tblAccession ON tblAccessionClinPending.UniqAccessionID = tblAccession.UniqAccessionID INNER JOIN tblPatientHeader ON tblAccession.UniqPatientID = tblPatientHeader.UniqPatientID INNER JOIN tblPatientDetail ON tblAccession.UniqPatDetailID = tblPatientDetail.UniqPatDetailID WHERE     (tblPatientDetail.LastName = '{0}') AND (tblPatientDetail.FirstName = '{1}') AND (tblPatientHeader.BirthDate = '{2}') AND  (tblAccessionClinPending.ClnCategoryID = '{3}') AND  (tblAccessionClinPending.Finalized = 0) AND (tblAccessionClinPending.Cancelled = 0) ORDER BY tblAccession.UniqAccessionID DESC"
        If objADOResults Is Nothing Then
            blnConnected = ConnectDatabase(objADOResults, strCunninghamConnect)
        End If
        If blnConnected Then
            Dim strSQL As String = String.Format(PatientAccessionLookupSQL, strLastname, strFirstName, strDOB, strClnCategoryId)
            Dim dtLookup As DataTable = objADOResults.GetDataTable(strSQL)

            If dtLookup.Rows.Count > 0 Then
                strReturn = dtLookup.Rows(0)!AccessionNumber
            End If
            dtLookup.Dispose()
        End If

        Return strReturn
    End Function
    Private Function ReturnTestElements(ByVal intMSH As Integer) As clsResultRecords
        Dim dtTemp As DataTable = objADOMessages.GetDataTable(ReturnTestDataSQL(intMSH))
        Dim objRecord As clsResultRecord = Nothing
        Dim objRecords As New clsResultRecords
        objRecords.intMSH = intMSH
        For I As Integer = 0 To dtTemp.Rows.Count - 1
            objRecord = New clsResultRecord
            objRecord.AccessionNumber = "" & dtTemp.Rows(I)!Accession
            objRecord.UniqAccessionID = Val("" & dtTemp.Rows(I)!UniqAccessionID)
            objRecord.TestPanelIdentifier = "" & dtTemp.Rows(I)!ObservationIdentifier
            objRecord.ResultText = "" & dtTemp.Rows(I)!Comment
            objRecords.Add(objRecord)
        Next

        dtTemp.Dispose()
        dtTemp = Nothing
        Return objRecords
    End Function
    Private Function ReturnTestDataSQL(ByVal intMSH As Integer) As String
        Dim strReturn = "SELECT tblMSH.MSH_Id, tblMSH.UniqAccessionID, tblMSH.Accession, tblOBX.OBX_Id, tblOBR.ObservationEndDateTime, tblOBX.SetIdOBX, tblOBX.ObservationIdentifier, " &
                        "tblNTE.NTE_Id, tblNTE.Comment FROM tblMSH INNER JOIN " &
                        "tblPID ON tblMSH.MSH_Id = tblPID.MSH_Id INNER JOIN " &
                        "tblOBR ON tblPID.PID_Id = tblOBR.PID_Id INNER JOIN " &
                        "tblOBX ON tblOBR.OBR_Id = tblOBX.OBR_Id INNER JOIN " &
                        "tblNTE ON tblOBX.OBX_Id = tblNTE.PAR_Id WHERE tblNTE.PAR_Type = 'OBX' and tblMSH.MSH_ID = " & intMSH
        Return strReturn
    End Function
#End Region

#Region "Cross Reference Panel/Test Lookup"
    Private Sub PopulatePanelTestFromXREF(ByRef objRecord As clsResultRecord)
        'Read Cross reference data search database.
        Dim aryTID() As String = objRecord.TestPanelIdentifier.Split("^")
        Dim strTestCheck As String
        Dim xlTranslatedTest As New xlateTestOrder
        xlTranslatedTest = ReturnPanelCode(aryTID(0))
        'Can this panel be a reflex panel
        'Check to see if we have a panel
        If xlTranslatedTest.ReturnCode = "" Then
            'No panel so check to see if this is a test
            If objRecord.ResultText <> "" Then
                strTestCheck = objRecord.ResultText.Substring(objRecord.ResultText.IndexOf("FOR") + 3).Trim()
                'strTemp = ReturnTestCode(strTestCheck)
                xlTranslatedTest = ReturnTestCode(strTestCheck)

            Else
                If aryTID.Length > 1 Then
                    strTestCheck = aryTID(1)
                Else
                    strTestCheck = aryTID(0)
                End If
                xlTranslatedTest = ReturnTestCode(strTestCheck)

            End If

            If xlTranslatedTest.ReturnCode = "" Then
                'No test or panel match for a the OBR
                'This objRecord needs to be Flagged
                objRecord.UpdateStatus = 3
            Else
                'Test Match the xref so return the test code
                objRecord.TestsResulted.Add(xlTranslatedTest.ReturnCode)
                objRecord.AllowRefex = xlTranslatedTest.Reflex
                objRecord.SingleTest = xlTranslatedTest.ReturnCode
                objRecord.UpdateStatus = 1
            End If
        Else
            'Panel Matched
            objRecord.Panel = xlTranslatedTest.ReturnCode
            objRecord.AllowRefex = xlTranslatedTest.Reflex
            Dim blnFound As Boolean
            Dim dtPanelTests As DataTable = objADOResults.GetDataTable(String.Format("Select ClnTestID from tblAccessionClinPending Where ClnGroupTestID = '{0}' and UniqAccessionID = {1}", objRecord.Panel, objRecord.UniqAccessionID))
            'breakdown the tests from the NTE line
            If dtPanelTests.Rows.Count = 0 And objRecord.AllowRefex = True Then
                'This will pull ALL Tests for that panel but only on a reflex panel that was NOT ordered by Cunningham.
                dtPanelTests = objADOResults.GetDataTable(String.Format("SELECT [ClnTestID] FROM [Cunningham].[dbo].[tblNBClnGroupingTest] Where clngrouptestid= '{0}'  Order by ClnGroupingTestSeq ", objRecord.Panel))
            End If

            Dim xlateTest As xlateTestOrder
            If objRecord.ResultText.StartsWith("POSITIVE") Then
                Dim aryRValues() As String = objRecord.ResultText.Replace("FOR", "^").Split("FOR")
                Dim aryPTID() As String = objRecord.ResultText.Substring(objRecord.ResultText.IndexOf("FOR") + 3).Trim().Split(",")
                'Now take the tests and cross reference them
                For I As Integer = 0 To aryPTID.Length - 1
                    xlateTest = ReturnTestCode(aryPTID(I).Trim())
                    'strTemp = ReturnTestCode(aryPTID(I).Trim())
                    If xlateTest.ReturnCode <> "" Then
                        blnFound = False
                        For T As Integer = 0 To objRecord.TestsResulted.Count - 1
                            If objRecord.TestsResulted(T).ToUpper() = xlateTest.ReturnCode.ToString.ToUpper() Then
                                blnFound = True
                                Exit For
                            End If
                        Next
                        If blnFound = False Then
                            objRecord.TestsResulted.Add(xlateTest.ReturnCode)
                        End If
                        objRecord.UpdateStatus = 1
                    Else
                        'No match for this record so flag
                        objRecord.UpdateStatus = 4
                    End If
                Next
            End If
            'this will only do the tests in the accession


            For I As Integer = 0 To dtPanelTests.Rows.Count - 1
                blnFound = False
                For T As Integer = 0 To objRecord.TestsResulted.Count - 1
                    If objRecord.TestsResulted(T).ToUpper() = ("" & dtPanelTests.Rows(I)!ClnTestID).ToString.ToUpper() Then
                        blnFound = True
                        Exit For
                    End If
                Next
                If blnFound = False Then
                    objRecord.TestsNegative.Add(("" & dtPanelTests.Rows(I)!ClnTestID).ToString)
                    objRecord.UpdateStatus = 1
                End If
            Next

        End If
    End Sub
    Private Function ReturnPanelCode(ByVal strValue As String) As xlateTestOrder ' Helper code for PopulatePanelTestFromXREF
        Dim xlReturn As New xlateTestOrder
        Dim strSQL As String = "SELECT ClnGroupTestID,AllowReflex FROM tblXlationInterfaceNBClnGroup  WHERE InterfaceID = " + ConfigurationManager.AppSettings.Item("InterfaceId") + " AND IntClnGroupTestID = '" & strValue & "' AND Direction = 0"
        Dim dtTemp As DataTable = objADOResults.GetDataTable(strSQL, "PanelXlate")
        If dtTemp.Rows.Count > 0 Then
            xlReturn.ReturnCode = ("" & dtTemp.Rows(0)!ClnGroupTestID).ToString.Trim()
            If IsDBNull(dtTemp.Rows(0)!AllowReflex) = False Then
                xlReturn.Reflex = dtTemp.Rows(0)!AllowReflex
            End If
        End If
        Return xlReturn
    End Function
    Private Function ReturnTestCode(ByVal strValue As String) As xlateTestOrder
        Dim xlReturn As New xlateTestOrder
        If strValue.ToUpper = "BACTERIAL VAGINOSIS ORGANISM" Then
            strValue = "BACTERIAL VAGINOSIS ORGANISMS"
        End If
        Dim strSQL As String = "Select ClnTestID, AllowReflex from tblXlationInterfaceNBClnTest Where InterfaceID = " + ConfigurationManager.AppSettings.Item("InterfaceId") + " and IntClnTestID = '" & strValue & "' and Direction = 0"
        Dim dtTemp As DataTable = objADOResults.GetDataTable(strSQL, "TestXlate")
        If dtTemp.Rows.Count > 0 Then
            xlReturn.ReturnCode = ("" & dtTemp.Rows(0)!ClnTestID).ToString.Trim()
            If IsDBNull(dtTemp.Rows(0)!AllowReflex) = False Then
                xlReturn.Reflex = dtTemp.Rows(0)!AllowReflex
            End If
        End If
        Return xlReturn
    End Function
#End Region
    Private Function ReturnMatchingAccessionIDforInterfaceHeader(ByVal strAccessionNumber As String, ByRef lngAccessionStatus As Long) As Tuples.Tuple(Of Integer, String)
        Dim strPrefix As String = String.Empty
        Dim strTestingLocationID As String = String.Empty
        Dim strPlaceOfService As String = String.Empty
        Dim strThisYear As String = Now.Year.ToString().Substring(2, 2)
        Dim strYear As String = String.Empty
        Dim strCentury = Now.Year.ToString().Substring(0, 2)
        Dim strSequence As String = String.Empty
        Dim strEval As String = String.Empty
        Dim intReturn As Integer = 0
        Dim T As Tuples.Tuple(Of Integer, String) = Nothing
        Dim s As String = strAccessionNumber.Substring(5, 1)

        If IsNumeric(s) Or s = "-" Then
            strEval = strAccessionNumber.Replace("-", "")

            If strEval.Substring(4, 2) <> strCentury Then
                If Integer.Parse(strEval.Substring(4, 2)) <= Integer.Parse(strThisYear) Then
                    strYear = strCentury & strEval.Substring(4, 2)
                    strSequence = strEval.Substring(6)
                Else
                    strYear = strCentury & strEval.Substring(4, 2)
                    strSequence = String.Empty
                End If
            Else
                strYear = strEval.Substring(4, 4)
                strSequence = strEval.Substring(8)
                If Integer.Parse(strYear) > Now.Year Then
                    strYear = String.Empty
                    strSequence = String.Empty
                End If
            End If
        Else
            Return Nothing
        End If

        strEval = strEval.Substring(0, 4) & strYear & strSequence

        If strEval.Length > 6 Then
            strPrefix = strEval.Substring(0, 1)
            strTestingLocationID = strEval.Substring(1, 2)
            strPlaceOfService = strEval.Substring(3, 1)
            If strPlaceOfService = "0" Then
                strPlaceOfService = "O"
            End If
            strSequence = strEval.Substring(8).PadLeft(6, "0")

            strEval = strPrefix & strTestingLocationID & strPlaceOfService & strYear & strSequence

            Dim strAccessionComponentSQL As String = "SELECT UniqAccessionID, AccessionStatus FROM tblAccession WHERE (PrefixID = '" & strPrefix & "') AND (TestingLocationID = '" & strTestingLocationID & "') AND (PlaceOfServiceID = '" & strPlaceOfService & "') AND (CaseYear = '" & strYear & "') AND (AccessionSequence = '" & strSequence & "')"
            Dim dtID As DataTable

            dtID = objADOResults.GetDataTable(strAccessionComponentSQL)

            If Not dtID.Rows.Count = 0 Then
                intReturn = dtID.Rows(0)!UniqAccessionID
                lngAccessionStatus = dtID.Rows(0)!AccessionStatus
                T = Tuples.Tuple.[New](intReturn, strEval)
            End If

            dtID.Dispose()
        End If

        Return T
    End Function

    'Private Function ReturnMatchingAccessionIDforInterfaceHeader(ByVal strAccessionNumber As String, ByRef lngAccessionStatus As Long) As Integer
    '    'Break it down. Then Look it up.
    '    Dim strPrefix As String
    '    Dim strTestingLocationID As String
    '    Dim strPlaceOfService As String
    '    Dim strYear As String = "2015"
    '    Dim strSequence As String
    '    Dim intReturn As Integer = 0
    '    Dim strEval As String
    '    'one lump or two?
    '    Dim aryDashes() As String = strAccessionNumber.Split("-")
    '    If aryDashes.Length = 1 Then 'No dashes
    '        strEval = aryDashes(0)
    '    ElseIf aryDashes.Length = 2 Then 'Only one dash present
    '        If aryDashes(0).Length < 8 Then 'assume two digit year
    '            strYear = aryDashes(0).Substring(4, 2)
    '            strYear = Now.Year.ToString.Substring(0, 2) & strYear
    '        Else
    '            strYear = aryDashes(0).Substring(4, 4)
    '        End If
    '        strEval = aryDashes(0).Substring(0, 4) & strYear & aryDashes(1)
    '    ElseIf aryDashes.Length = 3 Then 'two dashes
    '        If aryDashes(1).Length < 4 Then 'assume two digit year
    '            strYear = aryDashes(1)
    '            strYear = Now.Year.ToString.Substring(0, 2) & strYear
    '        Else
    '            strYear = aryDashes(1)
    '        End If
    '        strEval = aryDashes(0) & strYear & aryDashes(2)
    '    Else
    '        strEval = strAccessionNumber
    '    End If

    '    If strEval.Length > 6 Then
    '        strPrefix = strEval.Substring(0, 1)
    '        strTestingLocationID = strEval.Substring(1, 2)
    '        strPlaceOfService = strEval.Substring(3, 1)
    '        If strPlaceOfService = "0" Then
    '            strPlaceOfService = "O"
    '        End If
    '        strSequence = strEval.Substring(8).PadLeft(6, "0")
    '        Dim strAccessionComponentSQL As String = "SELECT UniqAccessionID, AccessionStatus FROM tblAccession WHERE (PrefixID = '" & strPrefix & "') AND (TestingLocationID = '" & strTestingLocationID & "') AND (PlaceOfServiceID = '" & strPlaceOfService & "') AND (CaseYear = '" & strYear & "') AND (AccessionSequence = '" & strSequence & "')"
    '        Dim dtID As DataTable
    '        dtID = objADOResults.GetDataTable(strAccessionComponentSQL)
    '        If Not dtID.Rows.Count = 0 Then
    '            intReturn = dtID.Rows(0)!UniqAccessionID
    '            lngAccessionStatus = dtID.Rows(0)!AccessionStatus
    '        End If
    '        dtID.Dispose()
    '    End If

    '    Return intReturn

    'End Function
    Private Function GetUniqAccessionID(ByVal strAccession As String) As Tuples.Tuple(Of Integer, String)
        'Dim intReturn As Integer
        Dim lngAccessionStatus As Long
        Dim T As Tuples.Tuple(Of Integer, String)
        T = ReturnMatchingAccessionIDforInterfaceHeader(strAccession, lngAccessionStatus)

        Return T 'intReturn
    End Function
    Private Sub WriteLogFile(ByVal strMessage As String)
        Dim strLogPath As String = System.IO.Path.GetDirectoryName(Application.ExecutablePath)
        If strLogPath.EndsWith("\") = False Then strLogPath = strLogPath & "\"
        Dim intFilenum As Integer
        Dim strLogFile As String
        If strLogPath <> "" Then
            strLogFile = strLogPath & "BHLResulting.log"
            intFilenum = FreeFile()
            Microsoft.VisualBasic.FileSystem.FileOpen(intFilenum, strLogFile, OpenMode.Append)
            Print(intFilenum, Now & " - " & strMessage & vbCrLf)
            Microsoft.VisualBasic.FileSystem.FileClose(intFilenum)
        End If
    End Sub

    Public Sub New()
        bolUpdateResultsOnly = ConfigurationManager.AppSettings.Get("UpdateResultsOnly")

        If bolUpdateResultsOnly = False Then
            If ConfigurationManager.AppSettings.Get("OpenFor") = "LiveServer" Then

                strCunninghamConnect = ConfigurationManager.ConnectionStrings("CunninghamLiveServer").ConnectionString
                strResultsConnect = ConfigurationManager.ConnectionStrings("CunninghamLiveServerResults").ConnectionString
                strDirectoryPoll = ConfigurationManager.AppSettings.Get("DirectoryPoll")
                strDirectoryProcessed = ConfigurationManager.AppSettings.Get("DirectoryProcessed")
                strDirectoryErrors = ConfigurationManager.AppSettings.Get("DirectoryErrors")
            End If
            If ConfigurationManager.AppSettings.Get("OpenFor") = "ReportServer" Then

                strCunninghamConnect = ConfigurationManager.ConnectionStrings("CunninghamReportServer").ConnectionString
                strResultsConnect = ConfigurationManager.ConnectionStrings("CunninghamReportServerResults").ConnectionString
                strDirectoryPoll = ConfigurationManager.AppSettings.Get("DirectoryPollTest")
                strDirectoryProcessed = ConfigurationManager.AppSettings.Get("DirectoryProcessedTest")
                strDirectoryErrors = ConfigurationManager.AppSettings.Get("DirectoryErrorsTest")
            End If

            If ConfigurationManager.AppSettings.Get("OpenFor") = "TestServer" Then
                strCunninghamConnect = ConfigurationManager.ConnectionStrings("CunninghamTestServer").ConnectionString
                strResultsConnect = ConfigurationManager.ConnectionStrings("CunninghamTestServerResults").ConnectionString
                strDirectoryPoll = ConfigurationManager.AppSettings.Get("DirectoryPollTest")
                strDirectoryProcessed = ConfigurationManager.AppSettings.Get("DirectoryProcessedTest")
                strDirectoryErrors = ConfigurationManager.AppSettings.Get("DirectoryErrorsTest")
            End If
        Else
            strCunninghamConnect = ConfigurationManager.ConnectionStrings("CunninghamReportServer").ConnectionString
            strResultsConnect = ConfigurationManager.ConnectionStrings("CunninghamReportServerResults").ConnectionString
            strDirectoryPoll = ConfigurationManager.AppSettings.Get("DirectoryPollTest")
            strDirectoryProcessed = ConfigurationManager.AppSettings.Get("DirectoryProcessedTest")
            strDirectoryErrors = ConfigurationManager.AppSettings.Get("DirectoryErrorsTest")
        End If
    End Sub

    'Public Sub SendEmail()
    '    ' change cursor into wait cursor
    '    Dim cr As Cursor = Cursor.Current
    '    Cursor.Current = Cursors.WaitCursor
    '    Dim mail As New SMTPEmailer.Emailer()
    '    Dim mailItem As New SMTPEmailer.EmailMessage

    '    Try
    '        mailItem.Subject = ConfigurationManager.AppSettings.Get("EmailSubject")
    '        mailItem.Body = ConfigurationManager.AppSettings.Get("EmailMessage")
    '        mailItem.From = ConfigurationManager.AppSettings.Get("EmailFrom")
    '        mailItem.To = ConfigurationManager.AppSettings.Get("EmailTo")
    '        'mail.EmailFrom = "Dennis Almond[dalmond@cunninghampathology.com]"
    '        'mail.EmailTo = "Dennis Almond[dalmond@cunninghampathology.com]"
    '        mailItem.Host = ConfigurationManager.AppSettings.Get("EmailHost")
    '        mailItem.CC = ","
    '        mail.SendOutEmail(mailItem)

    '    Catch e As Exception
    '    End Try
    'End Sub
End Class
