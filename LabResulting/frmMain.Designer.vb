<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnPoll = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.optionFiles = New System.Windows.Forms.RadioButton
        Me.optionDatabase = New System.Windows.Forms.RadioButton
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.stbStatusText = New System.Windows.Forms.ToolStripStatusLabel
        Me.stbProgress = New System.Windows.Forms.ToolStripProgressBar
        Me.tmrPoll = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnPoll
        '
        Me.btnPoll.Location = New System.Drawing.Point(347, 47)
        Me.btnPoll.Name = "btnPoll"
        Me.btnPoll.Size = New System.Drawing.Size(106, 38)
        Me.btnPoll.TabIndex = 0
        Me.btnPoll.Text = "Poll"
        Me.btnPoll.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optionFiles)
        Me.GroupBox1.Controls.Add(Me.optionDatabase)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(282, 77)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Import"
        '
        'optionFiles
        '
        Me.optionFiles.AutoSize = True
        Me.optionFiles.Location = New System.Drawing.Point(136, 32)
        Me.optionFiles.Name = "optionFiles"
        Me.optionFiles.Size = New System.Drawing.Size(46, 17)
        Me.optionFiles.TabIndex = 1
        Me.optionFiles.Text = "Files"
        Me.optionFiles.UseVisualStyleBackColor = True
        '
        'optionDatabase
        '
        Me.optionDatabase.AutoSize = True
        Me.optionDatabase.Checked = True
        Me.optionDatabase.Location = New System.Drawing.Point(16, 32)
        Me.optionDatabase.Name = "optionDatabase"
        Me.optionDatabase.Size = New System.Drawing.Size(71, 17)
        Me.optionDatabase.TabIndex = 0
        Me.optionDatabase.TabStop = True
        Me.optionDatabase.Text = "Database"
        Me.optionDatabase.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stbStatusText, Me.stbProgress})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 152)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(468, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'stbStatusText
        '
        Me.stbStatusText.AutoSize = False
        Me.stbStatusText.Name = "stbStatusText"
        Me.stbStatusText.Size = New System.Drawing.Size(351, 17)
        Me.stbStatusText.Spring = True
        Me.stbStatusText.Text = "Ready."
        Me.stbStatusText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'stbProgress
        '
        Me.stbProgress.Name = "stbProgress"
        Me.stbProgress.Size = New System.Drawing.Size(100, 16)
        '
        'tmrPoll
        '
        Me.tmrPoll.Interval = 60000
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(468, 174)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnPoll)
        Me.Name = "frmMain"
        Me.Text = "Lab Result"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPoll As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optionFiles As System.Windows.Forms.RadioButton
    Friend WithEvents optionDatabase As System.Windows.Forms.RadioButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents stbProgress As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents stbStatusText As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tmrPoll As System.Windows.Forms.Timer

End Class
