Imports System.Configuration

Public Class frmMain
    Private WithEvents objLab As clsLabResulting
    Private polInterval As Integer = 0

    Private Sub Poll()
        objLab = New clsLabResulting

        If optionDatabase.Checked Then
            objLab.PollDatabase()
        Else
            objLab.PollFiles()
        End If
    End Sub

    Private Sub btnPoll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPoll.Click
        Poll()
    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If My.Application.CommandLineArgs.Count > 0 Then
            objLab = New clsLabResulting
            If My.Application.CommandLineArgs(0).ToUpper = "FILES" Then
                objLab.PollFiles()
            ElseIf My.Application.CommandLineArgs(0).ToUpper = "DATABASE" Then
                objLab.PollDatabase()
            End If
            objLab = Nothing
            End
        Else
            If My.Settings.ImportType = "FILES" Then
                optionFiles.Checked = True
            Else
                optionDatabase.Checked = True
            End If
            polInterval = ConfigurationManager.AppSettings.Get("PollInterval")
            tmrPoll.Interval = polInterval
            tmrPoll.Start()
            UpdateNextPollStatus()
        End If
    End Sub

#Region "Status Events"

    Private Sub objLab_StatusStart(ByVal intTotal As Integer) Handles objLab.StatusStart
        stbProgress.Maximum = intTotal
        stbProgress.Minimum = 0
        stbStatusText.Text = "Starting Import"
    End Sub

    Private Sub objLab_StatusUpdate(ByVal intCount As Integer, ByVal strDesc As String) Handles objLab.StatusUpdate
        stbProgress.Value = intCount
        stbStatusText.Text = strDesc
    End Sub

    Private Sub objLab_StatusEnd() Handles objLab.StatusEnd
        stbStatusText.Text = "Process Complete."
        stbProgress.Value = 0
    End Sub
#End Region

    Private Sub tmrPoll_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrPoll.Tick
        tmrPoll.Stop()
        Poll()
        tmrPoll.Start()
        UpdateNextPollStatus()
    End Sub
    Private Sub UpdateNextPollStatus()
        Dim polUpdateInterval As Integer = polInterval / 60000
        stbStatusText.Text = "Next Poll Time: " & DateAdd(DateInterval.Minute, polUpdateInterval, Date.Now).ToString("MM/dd/yyyy hh:mm:ss")
    End Sub
End Class
