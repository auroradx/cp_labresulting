Public Class clsResultRecord
    Public AccessionNumber As String 'Raw HL7
    Public UniqAccessionID As Integer 'Raw HL7
    Public TestPanelIdentifier As String 'Raw HL7
    Public ResultText As String 'Raw HL7
    Public ResultValue As String 'The actual Negative or Positive
    Public Panel As String 'Resolved Panel name from Xref
    Public SingleTest As String 'Resolved Test Name from Xref
    Public TestsResulted As New List(Of String) ' Resolved Panel Tests names from Xref
    Public TestsNegative As New List(Of String) ' Resolved Panel Tests names from Xref
    Public UpdateStatus As Integer 'Status of test, could it be updated, missing xref, etc.
    Public PID_Id As Integer
    Public FillerOrderNumber As String
    Public AllowRefex As Boolean
    'Public MSHRecordList As List(Of List(Of clsUpDateRecord))
End Class

Public Class clsResultRecords
    Inherits List(Of clsResultRecord)
    Public intMSH As Integer
    Public strFileName As String
End Class
